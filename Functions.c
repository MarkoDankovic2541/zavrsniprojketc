#define _CRT_SECURE_NO_WARNINGS
#include "Header.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

int vracaBrojRadnika() {

	int brojRadnika;
	printf("Unesite broj zaposlenika u skoli: ");
	scanf("%d", &brojRadnika);
	return brojRadnika;
}

void unosPodataka() {

	int i, pomocna = 0;
	ZAPOSLENIK* radnik = NULL;
	FILE* fp = NULL;
	fp = fopen("BazaPodatakaZaposlenika.bin", "r+b");
	if (fp == NULL) {
		fp = fopen("BazaPodatakaZaposlenika.bin", "wb");
		fread(&pomocna, sizeof(int), 1, fp);
		int brojRadnika = vracaBrojRadnika();
		pomocna += brojRadnika;

		radnik = (ZAPOSLENIK*)malloc(brojRadnika * sizeof(ZAPOSLENIK));
		if (radnik == NULL) {
			perror("Struktura nije alocirana");
			exit(EXIT_FAILURE);
		}
		system("cls");
		for (i = 0; i < brojRadnika; i++) {
			system("cls");
			printf("Ukupno zaposlenika: %d\nUnos %d. / %d zaposlenika:\n", pomocna, i + 1, brojRadnika);
			printf("\nIme %d. radnika: ", i + 1); getchar();
			fgets((radnik + i)->ime, 29, stdin);
			printf("Prezime %d. radnika: ", i + 1);
			fgets((radnik + i)->prezime, 29, stdin);
			printf("OIB %d. radnika: ", i + 1);
			fgets((radnik + i)->OIB, 11, stdin);
			printf("Placa %d. radnika: ", i + 1);
			scanf("%f", &(radnik + i)->placa);
			printf("\n");
		}
		rewind(fp);		
		fwrite(&pomocna, sizeof(int), 1, fp);
		fwrite(radnik, sizeof(ZAPOSLENIK), brojRadnika, fp);
		fclose(fp);
		free(radnik);
		return;
	}
	else {
		fread(&pomocna, sizeof(int), 1, fp);
		int brojRadnika = vracaBrojRadnika();
		pomocna += brojRadnika;

		radnik = (ZAPOSLENIK*)malloc(brojRadnika * sizeof(ZAPOSLENIK));
		if (radnik == NULL) {
			perror("Struktura nije alocirana");
			exit(EXIT_FAILURE);
		}
		system("cls");
		rewind(fp);
		fwrite(&pomocna, sizeof(int), 1, fp);
		for (i = 0; i < brojRadnika; i++) {
			system("cls");
			printf("Ukupno zaposlenika: %d\nUnos %d. / %d zaposlenika:\n", pomocna, i + 1, brojRadnika);
			printf("\nIme %d. radnika: ", (pomocna - brojRadnika + 1)); getchar();
			fgets((radnik + i)->ime, 29, stdin);
			printf("Prezime %d. radnika: ", i + 1);
			fgets((radnik + i)->prezime, 29, stdin);
			printf("OIB %d. radnika: ", i + 1);
			fgets((radnik + i)->OIB, 11, stdin);
			printf("Placa %d. radnika: ", i + 1);
			scanf("%f", &(radnik + i)->placa);
			printf("\n");
		}
		fseek(fp, sizeof(int) + ((pomocna - brojRadnika) * sizeof(ZAPOSLENIK)), SEEK_SET);
		fwrite(radnik, sizeof(ZAPOSLENIK), brojRadnika, fp);
		fclose(fp);
		free(radnik);
		return;
	}
}

void prosjekPlaca() {

	FILE* fp = NULL;
	fp = fopen("BazaPodatakaZaposlenika.bin", "rb");
	if (fp == NULL) {
		perror("Otvaranje BPZ.bin");
		exit(EXIT_FAILURE);
	}
	float prosjek = 0.0f;
	int nekibroj;
	fread(&nekibroj, sizeof(int), 1, fp);

	ZAPOSLENIK* radnik = NULL;
	radnik = (ZAPOSLENIK*)malloc(nekibroj * sizeof(ZAPOSLENIK));
	if (radnik == NULL) {
		perror("Alociranje strukture");
		exit(EXIT_FAILURE);
	}
	fread(radnik, sizeof(ZAPOSLENIK), nekibroj, fp);
	for (int i = 0; i < nekibroj; i++) {
		prosjek += (radnik + i)->placa;
	}
	system("cls");
	printf("\nProsjek placa u skoli je: %f\n", prosjek /= nekibroj);
	_getch();
	fclose(fp);
	free(radnik);
	return;
}

void minPlaca() {

	FILE* fp = NULL;
	fp = fopen("BazaPodatakaZaposlenika.bin", "rb");
	if (fp == NULL) {
		perror("Otvaranje BPZ.bin");
		exit(EXIT_FAILURE);
	}
	float prosjek = 0.0f;
	int nekibroj;
	fread(&nekibroj, sizeof(int), 1, fp);

	ZAPOSLENIK* radnik = NULL;
	radnik = (ZAPOSLENIK*)malloc(nekibroj * sizeof(ZAPOSLENIK));
	if (radnik == NULL) {
		perror("Alociranje strukture");
		exit(EXIT_FAILURE);
	}
	fread(radnik, sizeof(ZAPOSLENIK), nekibroj, fp);
	float min;
	min = (radnik + 0)->placa;
	for (int i = 0; i < nekibroj; i++) {
		if (min > (radnik + i)->placa)
			min = (radnik + i)->placa;
	}
	system("cls");
	printf("Najmanja placa u skoli iznosi: %f\n", min);
	_getch();
	fclose(fp);
	free(radnik);
	return;
}

void maxPlaca() {

	FILE* fp = NULL;
	fp = fopen("BazaPodatakaZaposlenika.bin", "rb");
	if (fp == NULL) {
		perror("Otvaranje BPZ.bin");
		exit(EXIT_FAILURE);
	}
	float prosjek = 0.0f;
	int nekibroj;
	fread(&nekibroj, sizeof(int), 1, fp);

	ZAPOSLENIK* radnik = NULL;
	radnik = (ZAPOSLENIK*)malloc(nekibroj * sizeof(ZAPOSLENIK));
	if (radnik == NULL) {
		perror("Alociranje strukture");
		exit(EXIT_FAILURE);
	}
	fread(radnik, sizeof(ZAPOSLENIK), nekibroj, fp);
	float max;
	max = (radnik + 0)->placa;
	for (int i = 0; i < nekibroj; i++) {
		if (max < (radnik + i)->placa)
			max = (radnik + i)->placa;
	}
	system("cls");
	printf("Najveca placa u skoli iznosi: %f\n", max);
	_getch();
	fclose(fp);
	free(radnik);
	return;
}

void ispisRadnika() {

	FILE* fp = NULL;
	fp = fopen("BazaPodatakaZaposlenika.bin", "rb");
	if (fp == NULL) {
		perror("Otvaranje BPZ.bin");
		exit(EXIT_FAILURE);
	}
	int nekibroj;
	rewind(fp);
	fread(&nekibroj, sizeof(int), 1, fp);

	ZAPOSLENIK* radnik = NULL;
	radnik = (ZAPOSLENIK*)malloc(nekibroj * sizeof(ZAPOSLENIK));
	if (radnik == NULL) {
		perror("Alociranje strukture");
		exit(EXIT_FAILURE);
	}
	system("cls");
	fread(radnik, sizeof(ZAPOSLENIK), nekibroj, fp);
	fclose(fp);
	for (int i = 0; i < nekibroj; i++) {
		printf("\n\t%d. RADNIK\n\n", i + 1);
		printf("IME: %s", (radnik + i)->ime);
		printf("PREZIME: %s", (radnik + i)->prezime);
		printf("OIB: %s", (radnik + i)->OIB);
		printf("PLACA: %f\n", (radnik + i)->placa);
	}
	printf("\n\tKRAJ ISPISA! Povratak...\n");
	_getch();
	free(radnik);
	return;
}

void izlazakIzProg() {

	char odgovor;
	while (1) {
		system("cls");
		getchar();
		printf("Jeste li sigurni da zelite izaci iz programa? (Y/N): ");
		scanf("%c", &odgovor);
		if (odgovor == 'Y')
			exit(EXIT_SUCCESS);
		else if (odgovor == 'N') {
			system("cls");
			printf("Povratak na izbornik.");
			_getch();
			return;
		}
		else {
			system("cls");
			printf("Krivi unos!");
			_getch();
		}
	}
}

void pretraga() {

	int odabir, brojRadnika, i;

	do {
		system("cls");
		printf("\nPretraga po:\n\n(1)Imenu\n(2) OIB-u\n(3) Povratak\n\nOdabir: ");
		scanf(" %d", &odabir);
	} while (odabir < 1 || odabir > 3);

	FILE* pretraga = NULL;
	pretraga = fopen("BazaPodatakaZaposlenika.bin", "rb");
	if (pretraga == NULL) {
		perror("File error");
		exit(EXIT_FAILURE);
	}

	ZAPOSLENIK* radnik = NULL;
	fread(&brojRadnika, sizeof(int), 1, pretraga);
	radnik = (ZAPOSLENIK*)malloc(brojRadnika * sizeof(ZAPOSLENIK));
	if (radnik == NULL) {
		perror("Struct error");
		exit(EXIT_FAILURE);
	}

	switch (odabir) {
	case 1: {
		int flag = 0;
		char* traziIme;
		traziIme = (char*)malloc(20 * sizeof(char));
		if (traziIme == NULL) {
			perror("Alokacija memorije");
			exit(EXIT_FAILURE);
		}
		system("cls");
		getchar();
		printf("\nUnesite ime: ");
		fgets(traziIme, 29, stdin);
		fseek(pretraga, sizeof(int), SEEK_SET);
		fread(radnik, sizeof(ZAPOSLENIK), brojRadnika, pretraga);
		for (i = 0; i < brojRadnika; i++) {
			if (strcmp((radnik + i)->ime, traziIme) == 0) {
				printf("\n\t%d. RADNIK\n\n", i + 1);
				printf("IME: %s", (radnik + i)->ime);
				printf("PREZIME: %s", (radnik + i)->prezime);
				printf("OIB: %s", (radnik + i)->OIB);
				printf("PLACA: %f\n", (radnik + i)->placa);
				flag = 1;
				_getch();
				break;
			}
		}
		if (flag == 0) {
			printf("\nNije pronadnje zaposlenik: %s", traziIme);
			_getch();
		}
		free(radnik);
		free(traziIme);
		fclose(pretraga);
		return;
	}
	case 2: {
		int flag = 0;
		char* traziOIB;
		traziOIB = (char*)malloc(12 * sizeof(char));
		if (traziOIB == NULL) {
			perror("Alokacija memorije");
			exit(EXIT_FAILURE);
		}
		system("cls");
		getchar();
		printf("\nUnesite OIB: ");
		fgets(traziOIB, 11, stdin);
		fseek(pretraga, sizeof(int), SEEK_SET);
		fread(radnik, sizeof(ZAPOSLENIK), brojRadnika, pretraga);
		for (i = 0; i < brojRadnika; i++) {
			if (strcmp((radnik + i)->OIB, traziOIB) == 0) {
				printf("\n\t%d. RADNIK\n\n", i + 1);
				printf("IME: %s", (radnik + i)->ime);
				printf("PREZIME: %s", (radnik + i)->prezime);
				printf("OIB: %s", (radnik + i)->OIB);
				printf("PLACA: %f\n", (radnik + i)->placa);
				flag = 1;
				_getch();
				break;
			}
		}
		if (flag == 0) {
			printf("\nNije pronadnje zaposlenik: %s", traziOIB);
			_getch();
		}
		free(radnik);
		free(traziOIB);
		fclose(pretraga);
		return;
	}
	}


}

void brisanjeZaposlenika() {

	int izbor, nekibroj;

	FILE* fp = NULL;
	fp = fopen("BazaPodatakaZaposlenika.bin", "rb");
	if (fp == NULL) {
		perror("Otvaranje BPZ.bin");
		exit(EXIT_FAILURE);
	}

	FILE* pomocni = NULL;
	pomocni = fopen("PomocnaBaza.bin", "wb");
	if (pomocni == NULL) {
		perror("Otvaranje BPZ.bin");
		exit(EXIT_FAILURE);
	}

	do {
		system("cls");
		printf("UKLANJANJE ZAPOSLENIKA\n(1)Po OIB-u\n(2)Povratak\nOdabir: ");
		scanf("%d", &izbor);
	} while (izbor < 1 || izbor > 2);

	switch (izbor) {
	case 1: {
		int pomocnaZapis = 0, pronadjeno = 0, i;
		char* trazeni_Oib;
		trazeni_Oib = (char*)malloc(12 * sizeof(char));
		if (trazeni_Oib == NULL) {
			perror("Alociranje memorije");
			exit(EXIT_FAILURE);
		}
		fread(&nekibroj, sizeof(int), 1, fp);
		ZAPOSLENIK* radnik = NULL;
		radnik = (ZAPOSLENIK*)malloc(nekibroj * sizeof(ZAPOSLENIK));
		if (radnik == NULL) {
			perror("Alociranje strukture");
			exit(EXIT_FAILURE);
		}
		getchar();
		printf("Unesite OIB za obrisati: ");
		fgets(trazeni_Oib, 11, stdin);

		pomocnaZapis = nekibroj - 1;
		fwrite(&pomocnaZapis, sizeof(int), 1, pomocni);

		for (i = 0; i < nekibroj; i++) {
			fread(radnik, sizeof(ZAPOSLENIK), 1, fp);
			if (strcmp(radnik->OIB, trazeni_Oib) == 0) {
				pronadjeno = 1;
			}
			else {
				fwrite(radnik, sizeof(ZAPOSLENIK), 1, pomocni);
			}
		}

		if (pronadjeno != 1) {
			system("cls");
			printf("\nNije pronadjen radnik sa unesenim OIB-om.\n");
			fclose(fp);
			fclose(pomocni);
			free(radnik);
			_getch();
		}
		else {
			system("cls");
			printf("\nObrisan!\n");
			_getch();
			fclose(fp);
			fclose(pomocni);
			free(radnik);
			remove("BazaPodatakaZaposlenika.bin");
			rename("PomocnaBaza.bin", "BazaPodatakaZaposlenika.bin");
		}
		return;
	}
	case 2:
		return;
	}
}

void sortiranje() {

	int sil_Uzl, broj_Zap, i;

	FILE* fp = NULL;
	fp = fopen("BazaPodatakaZaposlenika.bin", "rb");
	if (fp == NULL) {
		perror("Otvaranje BPZ.bin");
		exit(EXIT_FAILURE);
	}

	ZAPOSLENIK* radnik = NULL;
	fread(&broj_Zap, sizeof(int), 1, fp);
	radnik = (ZAPOSLENIK*)malloc(broj_Zap * sizeof(ZAPOSLENIK));
	if (radnik == NULL) {
		perror("Struct error");
		exit(EXIT_FAILURE);
	}

	do {
		system("cls");
		printf("\n\tSortiranje: \n\n[1] Uzlazno\n[2] Silazno\n\nIzbor: ");
		scanf(" %d", &sil_Uzl);
	} while (sil_Uzl < 1 || sil_Uzl > 2);

	fseek(fp, sizeof(int), SEEK_SET);
	fread(radnik, sizeof(ZAPOSLENIK), broj_Zap, fp);
	if (sil_Uzl == 1)
		qsort(radnik, broj_Zap, sizeof(ZAPOSLENIK), usporedbaPlaceUzl);
	else
		qsort(radnik, broj_Zap, sizeof(ZAPOSLENIK), usporedbaPlaceSil);
	system("cls");
	for (i = 0; i < broj_Zap; i++) {
		printf("\n\t%d. RADNIK\n\n", i + 1);
		printf("IME: %s", (radnik + i)->ime);
		printf("PREZIME: %s", (radnik + i)->prezime);
		printf("OIB: %s", (radnik + i)->OIB);
		printf("PLACA: %f\n", (radnik + i)->placa);
	}
	fprintf(stdout, "\nEnd of list! Continue...\n");
	_getch();
	fclose(fp);
	free(radnik);
	return;


}

int usporedbaPlaceUzl(const void* x, const void* y) {

	ZAPOSLENIK* placa_x = (ZAPOSLENIK*)x;
	ZAPOSLENIK* placa_y = (ZAPOSLENIK*)y;

	return (float)((placa_x->placa - placa_y->placa));
}

int usporedbaPlaceSil(const void* x, const void* y) {

	ZAPOSLENIK* placa_x = (ZAPOSLENIK*)x;
	ZAPOSLENIK* placa_y = (ZAPOSLENIK*)y;

	return (float)((placa_y->placa - placa_x->placa));
}