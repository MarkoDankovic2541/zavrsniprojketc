#ifndef HEADER_H
#define HEADER_H
#define _CRT_SECURE_NO_WARNINGS
typedef struct zaposlenik {
	char ime[20];
	char prezime[20];
	float placa;
	char OIB[12];
} ZAPOSLENIK;

int vracaBrojRadnika();
void unosPodataka();
void ispisRadnika();
void prosjekPlaca();
void minPlaca();
void maxPlaca();
void izlazakIzProg();
void pretraga();
void sortiranje();
void brisanjeZaposlenika();
int usporedbaPlaceUzl(const void*, const void*);
int usporedbaPlaceSil(const void*, const void*);
#endif

