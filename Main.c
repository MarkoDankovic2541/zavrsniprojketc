#include "Header.h"
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int main(void) {

	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	printf("|                                                                             |\n");
	printf("|                             PLACE U SKOLAMA                                 |\n");
	printf("|                                                       ~ Marko Dankovic, SR1 |\n");
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
	_getch();
	int opcija;
	while (1) {
		system("cls");
		printf("\nUnesite broj opcije koju zelite: \n\n");
		printf("(1)Dodavanje zaposlenika.\n");
		printf("(2)Ispis podataka o zaposlenicima.\n");
		printf("(3)Pretraga odredjenog zaposlenika.\n");
		printf("(4)Ispis prosjecne place u skoli.\n");
		printf("(5)Ispis minimalne place u skoli.\n");
		printf("(6)Ispis maksimalne place u skoli.\n");
		printf("(7)Brisanje zaposlenika.\n");
		printf("(8)Sortiranje zaposlenika.\n\n");
		printf("(9)Izlazak iz programa.\n\n");

		scanf("%d", &opcija);
		switch (opcija) {
		case 1:
			unosPodataka();
			break;
		case 2:
			ispisRadnika();
			break;
		case 3:
			pretraga();
			break;
		case 4:
			prosjekPlaca();
			break;

		case 5:
			minPlaca();
			break;

		case 6:
			maxPlaca();
			break;

		case 7:
			brisanjeZaposlenika();
			break;

		case 8:
			sortiranje();
			break;

		case 9:
			izlazakIzProg();
			break;
		}
	}

	return 0;
}
